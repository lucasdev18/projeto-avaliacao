package com.example.demo.entity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Medicamento {

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	@ManyToMany
	@JoinTable(
			  name = "medicamento_reacao", 
			  joinColumns = @JoinColumn(name = "medicamento_id"), 
			  inverseJoinColumns = @JoinColumn(name = "reacao_id")) 
	private List<Reacao> reacoes;
		
	@ManyToOne
	@JoinColumn(name="id_fabricante")
	private Fabricante fabricante;
	
	@JsonIgnore
	private String numeroRegistroAnvisa;
	
	@Transient
	private String numeroAnvisaMascara;
	
	@Transient
	private String telefoneMascara;
	
	@Transient
	private String dtValidade;
	
	private String nome;
	
	@JsonIgnore
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private Date validade;
	
	@JsonIgnore
	private String telefone;
	
	private int qtdComprimidos;

	private String preco;	

	public String getPreco() {
		return preco;
	}

	public void setPreco(String preco) {
		this.preco = preco;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public List<Reacao> getReacoes() {
		return reacoes;
	}

	public void setReacoes(List<Reacao> reacoes) {
		this.reacoes = reacoes;
	}

	public Fabricante getFabricante() {
		return fabricante;
	}
	
	public void setDtValidade(String dtValidade) {
		this.dtValidade = dtValidade;
	}

	public void setFabricante(Fabricante fabricante) {
		this.fabricante = fabricante;
	}

	public String getNumeroRegistroAnvisa() {
		return numeroRegistroAnvisa;
	}

	public void setNumeroRegistroAnvisa(String numeroRegistroAnvisa) {
		this.numeroRegistroAnvisa = numeroRegistroAnvisa;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getValidade() {
		return validade;
	}

	public void setValidade(Date validade) {
		this.validade = validade;
	}

	public String getTelefone() {
		return telefone;		
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public int getQtdComprimidos() {
		return qtdComprimidos;
	}

	public void setQtdComprimidos(int qtdComprimidos) {
		this.qtdComprimidos = qtdComprimidos;
	}
	
	public void setTelefoneMascara(String telefoneMascara) {
		this.telefoneMascara = telefoneMascara;
	}
	
	public String getDtValidade() {
		if(this.validade == null) {
			return dtValidade;
		}
		
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

		String date = simpleDateFormat.format(this.validade);
		
		return date.toString();
	}

	public String getNumeroAnvisaMascara() {
		if(this.numeroRegistroAnvisa == null) {
			return numeroAnvisaMascara;
		}
		
		return this.numeroRegistroAnvisa.substring(0,1)+"."+this.numeroRegistroAnvisa.substring(1,5)+"."+
		this.numeroRegistroAnvisa.substring(5,9)+"."+this.numeroRegistroAnvisa.substring(9,12)+"-"+
		this.numeroRegistroAnvisa.substring(12,13);
		
		
	}

	public void setNumeroAnvisaMascara(String numeroAnvisaMascara) {
		this.numeroAnvisaMascara = numeroAnvisaMascara;
	}

	public String getTelefoneMascara() {
		if(this.telefone == null) {
			return telefoneMascara;
		}
		
		return "("+telefone.substring(0,2)+")"+telefone.substring(2,6)+"-"+ telefone.substring(6,10);
				
	}
	

	
}
