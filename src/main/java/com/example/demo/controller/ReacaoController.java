package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Reacao;
import com.example.demo.repository.ReacaoRepository;

@RestController
public class ReacaoController {
			
	@Autowired 
	private ReacaoRepository repo;
		
	 	 
	@PostMapping("/reacao")
	public Reacao newReacao(@RequestBody Reacao newReacao) {		
		return this.repo.save(newReacao);				
	}
	 	 
	@PutMapping("/reacao/{id}")
	Reacao updateReacao(@RequestBody Reacao newReacao, @PathVariable Long id) {	   
		newReacao.setId(id);
		return this.repo.save(newReacao);
	}
	 	 
	@DeleteMapping("/reacao/{id}")
	void deleteReacao(@PathVariable Long id) {
	   this.repo.deleteById(id);
	}

}
