package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Medicamento;
import com.example.demo.repository.MedicamentoRepository;
import com.example.demo.usecase.BuscaNumeroAnvisa;
import com.example.demo.usecase.BuscaNumeroAnvisaImpl;
import com.example.demo.usecase.BuscaPorNome;
import com.example.demo.usecase.BuscaPorNomeImpl;
import com.example.demo.usecase.CreateMedicamento;
import com.example.demo.usecase.CreateMedicamentoImpl;
import com.example.demo.usecase.UpdateMedicamento;
import com.example.demo.usecase.UpdateMedicamentoImpl;

@RestController
public class MedicamentoController {
	
	@Autowired 
	private MedicamentoRepository repo;			 	 
	private final UpdateMedicamento up;	
	private final CreateMedicamento cr;	
	private final BuscaPorNome buscaNome;
	private final BuscaNumeroAnvisa buscaNumero;
	
		
	public MedicamentoController(UpdateMedicamentoImpl up, CreateMedicamentoImpl cr, BuscaPorNomeImpl b, BuscaNumeroAnvisaImpl bu) {
		this.up = up;
		this.cr = cr;
		this.buscaNome = b;
		this.buscaNumero = bu;
	}

	@PostMapping("/medicamento")
	public Medicamento newMedicamento(@RequestBody Medicamento newMedicamento) {		
		return this.cr.executar(newMedicamento);				
	}
	 	 
	@PutMapping("/medicamento/{id}")
	public Medicamento updateMedicamento(@RequestBody Medicamento newMedicamento, @PathVariable Long id) {	   
		//newMedicamento.setId(id);
		//return this.repo.save(newMedicamento);
		return this.up.executar(id, newMedicamento);
	}
	 	 
	@DeleteMapping("/medicamento/{id}")
	public void deleteReacao(@PathVariable Long id) {
	   this.repo.deleteById(id);
	}
	
	
	@GetMapping("/medicamento/searchNomeMedicamento")
	public Page<Medicamento> searchByNome(@RequestParam("searchTerm") String searchTerm,
			@RequestParam(
                    value = "page",
                    required = false,
                    defaultValue = "0") int page,
			@RequestParam(
                    value = "size",
                    required = false,
                    defaultValue = "10") int size
			){
		return this.buscaNome.executar(searchTerm, page, size);
	}
	
	
	@GetMapping("/medicamento/searchNumeroAnvisa")
	public Page<Medicamento> searchNumeroAnvisa(@RequestParam("searchTerm") String searchTerm,
			@RequestParam(
                    value = "page",
                    required = false,
                    defaultValue = "0") int page,
			@RequestParam(
                    value = "size",
                    required = false,
                    defaultValue = "10") int size
			){
		return this.buscaNumero.executar(searchTerm, page, size);
	}
	
	
	
	
}
