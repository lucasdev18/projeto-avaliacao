package com.example.demo.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Medicamento;

@Repository
public interface MedicamentoRepository extends JpaRepository<Medicamento, Long> {
	
	
	@Query("FROM Medicamento c " +
	           "WHERE LOWER(c.nome) like :searchTerm ")
    Page<Medicamento> search(
            @Param("searchTerm") String searchTerm, 
            Pageable pageable);
	
	
	@Query("FROM Medicamento c " +
	           "WHERE c.numeroRegistroAnvisa like :searchTerm ")
	 Page<Medicamento> searchNumeroRegistro(
	         @Param("searchTerm") String searchTerm, 
	         Pageable pageable);
	    
	
}
