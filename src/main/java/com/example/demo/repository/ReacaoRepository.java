package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Reacao;

@Repository
public interface ReacaoRepository extends JpaRepository<Reacao, Long> {
	
	
}
