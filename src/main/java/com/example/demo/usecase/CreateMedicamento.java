package com.example.demo.usecase;

import com.example.demo.entity.Medicamento;

public interface CreateMedicamento {
	
	public Medicamento executar(Medicamento newMedicamento);

}
