package com.example.demo.usecase;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.example.demo.entity.Medicamento;
import com.example.demo.exception.BusinessException;
import com.example.demo.repository.MedicamentoRepository;

@Component
public class BuscaNumeroAnvisaImpl implements BuscaNumeroAnvisa {

	@Autowired
	MedicamentoRepository mr;
	
	@Override
	public Page<Medicamento> executar(String nome, int page, int size) {
		// TODO Auto-generated method stub		
		
		String linha = nome;
		Pattern p = Pattern.compile("^([0-9]{1}).([0-9]{4}).([0-9]{4}).([0-9]{3})-([0-9]{1})");
		Matcher m = p.matcher(linha);
		String conteudo = "";
		try {
			m.matches();
			conteudo = m.group(1) + m.group(2) + m.group(3) + m.group(4) + m.group(5);
		}catch (Exception e) {
			throw new BusinessException("Número da anvisa com mascara incorreta", HttpStatus.FORBIDDEN);
		}
				
		PageRequest pageRequest = PageRequest.of(
                page,
                size,
                Sort.Direction.ASC,
                "numeroRegistroAnvisa");
		
		return mr.searchNumeroRegistro(conteudo, pageRequest);
	}
}
