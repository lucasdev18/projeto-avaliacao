package com.example.demo.usecase;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.example.demo.entity.Medicamento;
import com.example.demo.entity.Reacao;
import com.example.demo.exception.BusinessException;
import com.example.demo.repository.MedicamentoRepository;

@Component
public class CreateMedicamentoImpl implements CreateMedicamento {

	
	@Autowired 
	private MedicamentoRepository repo;
	
	
	@Override
	public Medicamento executar(Medicamento newMedicamento) {
		// TODO Auto-generated method stub
		
		String linha = newMedicamento.getNumeroAnvisaMascara();
		
		Pattern p = Pattern.compile("^([0-9]{1}).([0-9]{4}).([0-9]{4}).([0-9]{3})-([0-9]{1})");
		Matcher m = p.matcher(linha);
		String conteudo = "";
		
		try {
			m.matches();
			conteudo = m.group(1) + m.group(2) + m.group(3) + m.group(4) + m.group(5);
			
		}catch (Exception e) {
			throw new BusinessException("Número da anvisa com mascara incorreta", HttpStatus.FORBIDDEN);
		}
		
		newMedicamento.setNumeroRegistroAnvisa(conteudo);
		
		linha = newMedicamento.getDtValidade();
				
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

		String dateInString = linha;
		Date date = null;
		try {
			date = formatter.parse(dateInString);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		newMedicamento.setValidade(date);
		
		linha = newMedicamento.getTelefoneMascara();
		
		Pattern p1 = Pattern.compile("^\\(([0-9]{2})\\)([0-9]{4})-([0-9]{4})");
		Matcher m1 = p1.matcher(linha);
		conteudo = "";
				
		try {
			m1.matches();
			conteudo = m1.group(1) + m1.group(2) + m1.group(3);
			
		}catch (Exception e) {
			throw new BusinessException("formato do telefone incorreto ", HttpStatus.FORBIDDEN);
		}
		
		
		newMedicamento.setTelefone(conteudo);
						
		return this.repo.save(newMedicamento);		
	}

}
