package com.example.demo.usecase;

import com.example.demo.entity.Medicamento;
import com.example.demo.entity.Reacao;

public interface UpdateMedicamento {
	
	public Medicamento executar(Long id, Medicamento newMedicamento);

}
