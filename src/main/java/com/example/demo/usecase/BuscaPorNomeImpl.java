package com.example.demo.usecase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import com.example.demo.entity.Medicamento;
import com.example.demo.repository.MedicamentoRepository;


@Component
public class BuscaPorNomeImpl implements BuscaPorNome{

	
	@Autowired
	MedicamentoRepository mr;
	
	@Override
	public Page<Medicamento> executar(String nome, int page, int size) {
		// TODO Auto-generated method stub
		
		PageRequest pageRequest = PageRequest.of(
                page,
                size,
                Sort.Direction.ASC,
                "nome");
		
		return mr.search(nome, pageRequest);
	}

}
