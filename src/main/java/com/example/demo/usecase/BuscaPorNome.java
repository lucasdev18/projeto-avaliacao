package com.example.demo.usecase;

import org.springframework.data.domain.Page;

import com.example.demo.entity.Medicamento;

public interface BuscaPorNome {

	public Page<Medicamento> executar(String nome, int page, int size);
}
