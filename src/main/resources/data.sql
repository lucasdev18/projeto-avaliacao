INSERT INTO public.fabricante
    (id, nome)
SELECT 1, 'pfizer'
WHERE
    NOT EXISTS (
        SELECT id FROM public.fabricante WHERE id = 1
    );
    
INSERT INTO public.fabricante
    (id, nome)
SELECT 2, 'johnson'
WHERE
    NOT EXISTS (
        SELECT id FROM public.fabricante WHERE id = 2
    );